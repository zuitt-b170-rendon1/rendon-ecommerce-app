// React Boostrap Components
/*
    - Syntax
        - import moduleName from 'filePath';
*/
import { Fragment, useContext } from 'react';
import Navbar from 'react-bootstrap/Navbar';
import Nav from 'react-bootstrap/Nav';
import { Link, NavLink } from 'react-router-dom';
import UserContext from '../UserContext';


export default function AppNavbar() {

    const { user } = useContext(UserContext);

    return (
    
        <Navbar bg="white" expand="lg">
            <Navbar.Brand as={Link} to="/" >TVC</Navbar.Brand>
            <Navbar.Toggle aria-controls="basic-navbar-nav" />
            <Navbar.Collapse id="basic-navbar-nav">
                <Nav className="ml-auto">
                    <Nav.Link as={NavLink} to="/" exact >Home</Nav.Link>
                    <Nav.Link as={NavLink} to="/products" exact >Products</Nav.Link>
                    {(user.id !== null) ? 
                            <Nav.Link as={NavLink} to="/logout" exact >Logout</Nav.Link>
                        : 
                            <Fragment>
                                <Nav.Link as={NavLink} to="/login" exact >Login</Nav.Link>
                                <Nav.Link as={NavLink} to="/register" exact >Register</Nav.Link> 
                                <Nav.Link as={NavLink} to="/basket" exact >Basket</Nav.Link>
                            </Fragment>
                    }
                </Nav>
            </Navbar.Collapse>
        </Navbar>
    )
}

/*
==========================
Class Component Equivalent
==========================
*/

/*

import { Component } from 'react';
// React Boostrap Components
import Navbar from 'react-bootstrap/Navbar';
import Nav from 'react-bootstrap/Nav';

export default class AppNavbar extends Component {
    render() {
        return (
            <Navbar bg="light" expand="lg">
                <Navbar.Brand href="#home">Zuitt</Navbar.Brand>
                <Navbar.Toggle aria-controls="basic-navbar-nav" />
                <Navbar.Collapse id="basic-navbar-nav">
                    <Nav className="ml-auto">
                        <Nav.Link href="#home">Home</Nav.Link>
                        <Nav.Link href="#courses">Courses</Nav.Link>
                    </Nav>
                </Navbar.Collapse>
            </Navbar>
        )
    }
}

*/
