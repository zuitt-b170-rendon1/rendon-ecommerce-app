// Bootstrap grid system components
// import Row from 'react-bootstrap/Row';
// import Col from 'react-bootstrap/Col';
// import Card from 'react-bootstrap/Card';
import { Row, Col, Card } from 'react-bootstrap';


export default function Highlights() {
    return (

        <Row className="mt-3 mb-3">
        <h2 className="tt">Best Seller</h2>
            <Col xs={12} md={4} className="zz">
                <Card className="cardHighlight p-3">
                    <Card.Body>
                        <Card.Title>
                            <h2>Rage Concept</h2>
                        </Card.Title>
                        <Card.Text>
                            Breakfast Cereal 3mg
                        </Card.Text>
                    <img className= "pix" src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQxvSkADmupW06Fqse7NmoZabOEYvSDgjEBag&usqp=CAU" alt="Cereal">
                        </img>                         
                    </Card.Body>
                </Card>
            </Col>
            <Col xs={12} md={4}>
                <Card className="cardHighlight p-3">
                    <Card.Body>
                        <Card.Title>
                            <h2>Wicked</h2>
                        </Card.Title>
                        <Card.Text>
                            Yakult Flavor 3mg
                        </Card.Text>
                        <img className= "pix" src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSSJfKI3hkd4ajA29NmXWJLZaMh13--I6Szuw&usqp=CAU" alt="Yakult">
                        </img>                          
                    </Card.Body>
                </Card>
            </Col>
            <Col xs={12} md={4}>
                <Card className="cardHighlight p-3">
                    <Card.Body>
                        <Card.Title>
                            <h2>Espesyal</h2>
                        </Card.Title>
                        <Card.Text>
                           Cheesecake Flavor 6mg
                        </Card.Text>
                            <img className="pix" src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSen_TJNwcLzdrwoC4_6hzJn8OCsebM3cy9Kg&usqp=CAU" alt="cheesecake">
                            </img>                        
                    </Card.Body>
                </Card>
            </Col>
        </Row>
    )
}
