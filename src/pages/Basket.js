import React from 'react';

import { useState, useEffect, useContext } from 'react';
import { Form, Button } from 'react-bootstrap';
import { Redirect } from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';





export default function Basket(props) {

const [email, setEmail] = useState('');
const [vapeId, setVapeId] = useState('');


	return <aside className= "block col-1" class="background-color=dark">
		<h1>Cart Items</h1>
	</aside>

fetch(`http://gentle-castle-12098.herokuapp.com/api/order/checkout`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                vapeId: vapeId,
                
            })
        })
        .then(res => res.json())
        .then(data => {

            // It is good practice to always print out the result of our fetch request to ensure that the correct information is received in our frontend application
            console.log(data);

            // If no user information is found, the "access" property will not be available and will return undefined
            // Using the typeof operator will return a string of the data type of the variable/expression it preceeds which is why the value being compared is in a string data type
            if (typeof data.access !== "undefined") {

                // The JWT will be used to retrieve user information across the the whole frontend application and storing it in the localStorage will allow ease of access to the user's information
                localStorage.setItem('token', data.access);

                Swal.fire({
                    title: "Order Successful",
                 
                });

            } else {

                Swal.fire({
                    title: "Authentication failed",
                 
                });

            };

        })


}

