import { useState, useEffect, useContext } from 'react';
import { Container, Card, Button, Row, Col } from 'react-bootstrap';
import { useParams, useHistory, Link } from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';

export default function CourseView() {

	// The "useParams" hook allows us to retrieve any parameter or the courseId passed via the URL
	const { vapeId } = useParams();
	const { user } = useContext(UserContext);
	// Allows us to gain access to methods that will allow us to redirect a user to a different page after enrolling a course
	const history = useHistory();

	const [name, setName] = useState("");
	const [description, setDescription] = useState("");
	const [price, setPrice] = useState(0);
	const [addToCart, vape] = useState(0);
const [quantity, setQuantity] = useState(0)


		// Inc and Dec - start
	const handleDecrement = () => {
		if (quantity > 0) {
			setQuantity(prevCount => - 1)
		}
	}

	const handleIncrement = () => {
		if (quantity < 99) {
			setQuantity(prevCount => + 1)
		}
	}
// Inc and Dec - end	

	const enroll = (vapeId) => {

		 addToCart = () => {
        let cart = localStorage.getItem('cart') 
                      ? JSON.parse(localStorage.getItem('cart')) : {};
        let id = this.props.vape.id.toString();
        vape[id] = (vape[id] ? vape[id]: 0);
        let qty = vape[id] + parseInt(this.state.quantity);
        if (this.props.product.available_quantity < qty) {
          cart[id] = this.props.product.available_quantity; 
        } else {
          cart[id] = qty
        }
        localStorage.setItem('cart', JSON.stringify(cart));
      }

		fetch(`http://gentle-castle-12098.herokuapp.com/api/order/checkout`, {
			method: "POST",
			headers: {
				"Content-Type": "application/json",
				Authorization: `Bearer ${ localStorage.getItem('token') }`
			},
			body: JSON.stringify({
				vapeId: vapeId
			})
		})
		.then(res => res.json())
		.then(data => {

			console.log(data);

			if (data === true) {

				Swal.fire({
					title: "Successfully enrolled",
					icon: 'success',
					text: "You have successfully enrolled for this course."
				});

				// The "push" method allows us to redirect the user to a different page and is an easier approach rather than using the "Redirect" component
				history.push("/Products");

			} else {

				Swal.fire({
					title: "Something went wrong",
					icon: "error",
					text: "Please try again."
				});

			}

		});

	};



	useEffect(()=> {

		console.log(vapeId);

		fetch(`http://gentle-castle-12098.herokuapp.com/api/order/checkout`)
		.then(res => res.json())
		.then(data => {

			console.log(data);

			setName(data.name);
			setDescription(data.description);
			setPrice(data.price);

		});

	}, [vapeId]);

	
	return(




		<Container className="mt-5">
			<Row>
				<Col lg={{ span: 6, offset: 3 }}>
					<Card>
						<Card.Body className="text-center">
							<Card.Title>{name}</Card.Title>							
							<Card.Subtitle>Brand</Card.Subtitle>
							<Card.Text>Espesyal</Card.Text>
							<Card.Subtitle>Description:</Card.Subtitle>
							<Card.Text>Cheesecake Flavor 6mg</Card.Text>
							<Card.Subtitle>Price:</Card.Subtitle>
							<Card.Text>PhP 400</Card.Text>
							<img className= "pix" src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSen_TJNwcLzdrwoC4_6hzJn8OCsebM3cy9Kg&usqp=CAU" alt="cheesecake">
							</img>
							<br></br>
							
							<div className="form-control">{quantity}</div>
							<Col>
							<button type="button" onClick={handleIncrement} className="input-group-text">+</button>
							<button type="button" onClick={handleDecrement} className="input-group-text">-</button>
							</Col>
							{ user.id !== null ? 
									<Button variant="primary" block onClick={() => addToCart(vapeId)}>Add to Cart</Button>

								: 
									<Link className="btn btn-danger btn-block" to="/login">Log in to Order</Link>
							}
						</Card.Body>
					</Card>

					<Card>
						<Card.Body className="text-center">
							<Card.Title>{name}</Card.Title>							
							<Card.Subtitle>Brand</Card.Subtitle>
							<Card.Text>Wicked</Card.Text>
							<Card.Subtitle>Description:</Card.Subtitle>
							<Card.Text>Yakult Flavor 3mg</Card.Text>
							<Card.Subtitle>Price:</Card.Subtitle>
							<Card.Text>PhP 200</Card.Text>
							<img className= "pix" src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSSJfKI3hkd4ajA29NmXWJLZaMh13--I6Szuw&usqp=CAU" alt="Yakult">
							</img>	
							<br></br>		
							<div className="form-control">{quantity}</div>
							<Col>
							<button type="button" onClick={handleIncrement} className="input-group-text">+</button>
							<button type="button" onClick={handleDecrement} className="input-group-text">-</button>
							</Col>											
							{ user.id !== null ? 
									<Button variant="primary" block onClick={() => enroll(vapeId)}>Add to Cart</Button>
                	//localStorage.setItem('token', data.access);
                	//retrieveUserDetails(data.access);									
								: 
									<Link className="btn btn-danger btn-block" to="/login">Log in to Order</Link>
							}
						</Card.Body>
					</Card>

					<Card>
						<Card.Body className="text-center">
							<Card.Title>{name}</Card.Title>							
							<Card.Subtitle>Brand</Card.Subtitle>
							<Card.Text>Katas</Card.Text>
							<Card.Subtitle>Description:</Card.Subtitle>
							<Card.Text>Nutty RY4 Flavor 6mg</Card.Text>
							<Card.Subtitle>Price:</Card.Subtitle>
							<Card.Text>PhP 300</Card.Text>
							<img className= "pix" src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcREBaLSNbHGORiAfTWNUhmJIVgfNoPWX5CyV34WVV65WRG2zy2nKGkMZnY_gxtizjbP90Y&usqp=CAU" alt="Yakult">
							</img>
							<br></br>		
							<div className="form-control">{quantity}</div>
							<Col>
							<button type="button" onClick={handleIncrement} className="input-group-text">+</button>
							<button type="button" onClick={handleDecrement} className="input-group-text">-</button>
							</Col>													
							{ user.id !== null ? 
									<Button variant="primary" block onClick={() => enroll(vapeId)}>Add to Cart</Button>
								: 
									<Link className="btn btn-danger btn-block" to="/login">Log in to Order</Link>
							}
						</Card.Body>
					</Card>	

					<Card>
						<Card.Body className="text-center">
							<Card.Title>{name}</Card.Title>							
							<Card.Subtitle>Brand</Card.Subtitle>
							<Card.Text>Rage Concept</Card.Text>
							<Card.Subtitle>Description:</Card.Subtitle>
							<Card.Text>Breakfast Cereal 3mg</Card.Text>
							<Card.Subtitle>Price:</Card.Subtitle>
							<Card.Text>PhP 200</Card.Text>
							<img className= "pix" src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQxvSkADmupW06Fqse7NmoZabOEYvSDgjEBag&usqp=CAU" alt="Yakult">
							</img>
							<br></br>	
							<div className="form-control">{quantity}</div>
							<Col>
							<button type="button" onClick={handleIncrement} className="input-group-text">+</button>
							<button type="button" onClick={handleDecrement} className="input-group-text">-</button>
							</Col>														
							{ user.id !== null ? 
									<Button variant="primary" block onClick={() => enroll(vapeId)}>Add to Cart</Button>
								: 
									<Link className="btn btn-danger btn-block" to="/login">Log in to Order</Link>
							}
						</Card.Body>
					</Card>	


				</Col>
			</Row>
		</Container>
	)
}
