import { Fragment } from 'react';
import Banner from '../components/Banner';
import Highlights from '../components/Highlights';

export default function Home() {

	const data = {
	    title: "The Vape Club",
	    content: "Quality Vape Juice",
	    destination: "/Products",
	    label: "Order now!"
	}

	return (
		<Fragment>
			<Banner data={data}/>
			<Highlights />
		</Fragment>
	)

}
